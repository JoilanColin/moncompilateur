			# This code was produced by the CERI Compiler
	.data
	.align 8
z:	.quad 0
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
	push $0
	pop z
WHILE1:
	push z
	push $5
	pop %rax
	pop %rbx
	cmpq %rax, %rbx
	jbe Vrai2	# If below or equal
	push $0		# False
	jmp Suite2
Vrai2:	push $0xFFFFFFFFFFFFFFFF		# True
Suite2:
	pop %rax
	cmpq $0, %rax
	je ENDWHILE1
DO1:
	push z
	push $1
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop z
ENDWHILE1:
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
